def function_modify_not_covered():
    a = 2 + 3
    return a

def function_modify_covered():
    a = 2 + 3
    return a

def function_modify_badcode():
    this_line_makes_no_sense()
