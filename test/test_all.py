import sys
from pathlib import Path
import unittest
sys.path.append(str(Path(__file__).parent.parent))

from src.delete import function_delete_covered
from src.modify import function_modify_covered
from src.rename_before import function_rename_covered

class Tests(unittest.TestCase):
    def test_delete(self):
        self.assertEqual(function_delete_covered(), 5)
    
    def test_modify(self):
        self.assertEqual(function_modify_covered(), 5)

    def test_rename(self):
        self.assertEqual(function_rename_covered(), 5)